#!/usr/bin/env python3

from loguru import logger
import yaml
import os
#import sys

#sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
logger.add('log/inventory.log', format="{time} {level} | {module}:{function}| {message}", level="DEBUG")

file_path = os.path.dirname(os.path.realpath(__file__))

@logger.catch
def get_inventory():
    try:
        with open(file_path + "/inventory.yml", "r") as inv:
            inventory_list = yaml.safe_load(inv.read())
        logger.success("Retrieved inventory list successfully")
        return inventory_list

    except Exception as err:
        logger.error("Error retrieving inventory list {}", err)
        return err


if __name__ == "__main__":
    get_inventory()