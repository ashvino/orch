#!/usr/bin/env python3

import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

import pytest
from junos_api import junos

import json
from lxml import etree

DEVICE = 'R1'
USERNAME = 'sky'
PASSWORD = 'Sky@321'

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True


def test_connect_valid_cred():
    dev = junos(DEVICE, USERNAME, PASSWORD)
    assert dev.conn_status == True,"Failed"
    dev.disconnect()
    assert dev.conn_status == False,"Failed state"

def test_connect_wrong_cred():
    dev = junos('R1', 'test', 'test')
    assert dev.conn_status == False, "Failed"


def test_connect_wrong_device():
    dev = junos("R2", USERNAME, PASSWORD)
    assert dev.conn_status == False,"Failed"


def test_get_interface_info():
    dev = junos(DEVICE, USERNAME, PASSWORD)
    assert dev.conn_status == True,"Failed"
    state, interface_info_all = dev.get_interface_info()
    assert state == True,"Failed: Unable to get_interface_info"
    assert isinstance(interface_info_all, dict) == True,"Failed"

    state, interface_info_all_xml = dev.get_interface_info(data_format='xml')
    assert state == True,"Failed: Unable to get interface info in xml"
    assert isinstance(etree.tounicode(interface_info_all_xml), str) == True,"Failed - XML Output for get_interface is invalid"

    state, interface_info_all_xml = dev.get_interface_info(data_format='xml')
    assert state == True,"Failed: Unable to get interface info in xml"
    assert isinstance(etree.tounicode(interface_info_all_xml), str) == True,"Failed - XML Output for get_interface is invalid"

    state, interface_info_filter = dev.get_interface_info(interfaceName='ge-0/0/1')
    assert state == True,"Failed: Unable to get interface info with filter"
    assert len(interface_info_filter['interface-information'][0]['physical-interface']) == 1,"Failed. Length of interface filter > 1"

    dev.disconnect()
    assert dev.conn_status == False,"Failed state"


def test_get_configuration():
    dev = junos(DEVICE, USERNAME, PASSWORD)
    assert dev.conn_status == True,"Failed: Unable to connect"
    state, config = dev.get_configuration()
    assert state == True,"Failed: Unable to get config"
    assert isinstance(config, str) == True, "Failed: Config is not string"

    dev.disconnect()
    