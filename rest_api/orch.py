#!/usr/bin/env python3
from aiohttp import web
import os
import sys
import json
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from inventory.inventory import get_inventory
from junos_api import junos
from config_render import ConfigGen
from loguru import logger

logger.add('log/rest_api.log', format="{time} {level} | {module}:{function}| {message}", level="DEBUG")

routes = web.RouteTableDef()

USERNAME= 'sky'
PASSWORD = 'Sky@321'


def _check_inventory(device):
    inventory_list = get_inventory()
    if device in inventory_list['devices']:
        return True, inventory_list['devices']
    else:
        return False, inventory_list['devices']


@routes.get('/')
async def handler(request):
    return web.Response(text="Hello world")


@routes.get('/{device}/interfaces')
async def router_interfaces(request: web.Request) -> web.Response:
    device = request.match_info.get("device", "")
    try:
        detail_level = request.query['detail']
    except:
        detail_level = False

    try:
        interface_name = request.query['interface']
    except:
        interface_name = None

    try:
        format = request.query['format']
    except:
        format = 'json'

    inventory_list = get_inventory()
    if device in inventory_list['devices']:
        try:
            dev = junos(device, USERNAME, PASSWORD)
            if interface_name:
                result_state, interface_info = dev.get_interface_info(interfaceName=interface_name, data_format=format, terse=not(detail_level))
            else:
                result_state, interface_info = dev.get_interface_info(data_format=format, terse=not(detail_level))
            dev.disconnect()

            if result_state:
                return web.json_response({"interface_info": interface_info}, status=200)
            else:
                return web.json_response({'Error':'Interface information not found'}, status=404)

        except Exception as err:
            return web.json_response({"Error": err[1]}, status=500)
    else:
        return web.json_response({"Error":"Device does not exist in inventory"}, status=404)


@routes.get('/{device}/interfaces/interface')
async def router_interface_name(request: web.Request) -> web.Response:
    device = request.match_info.get("device", "")

    try:
        detail_level = request.query['detail']
    except:
        detail_level = False
    TERSE = not(detail_level)

    try:
        interface_name = request.query['name']
    except:
        interface_name = ""

    inventory_list = get_inventory()
    if device in inventory_list['devices']:
        try:
            dev = junos(device, USERNAME, PASSWORD)
            interface_info = dev.get_interface_info(interfaceName=interface_name, terse=not(detail_level))
            #interface_info = dev.get_interface_info(interfaceName=interface_name, terse=TERSE)
            dev.disconnect()
            return web.json_response(interface_info[1], status=200)
        except Exception as err:
            return web.json_response({"Error": err[1]}, status=500)
    else:
        return web.json_response({"Error":"Device does not exist in inventory"}, status=404)


@routes.get('/{device}/configuration')
async def router_interface_name(request: web.Request) -> web.Response:
    device = request.match_info.get("device", "")

    try:
        interface_name = request.query['interface']
    except:
        interface_name = None

    try:
        format = request.query['format']
    except:
        format = 'text'

    inventory_list = get_inventory()
    if device in inventory_list['devices']:
        try:
            dev = junos(device, USERNAME, PASSWORD)
            if interface_name:
                result_state, configuration = dev.get_configuration(interfaceName=interface_name, data_format=format)
            else:
                result_state, configuration = dev.get_configuration(data_format=format)
            dev.disconnect()
            if result_state:
                return web.json_response({'configuration': configuration}, status=200)
            else:
                return web.json_response({'Error':'Configuration not found'}, status=404)
        except Exception as err:
            return web.json_response({"Error": err[1]}, status=500)
    else:
        return web.json_response({"Error":"Device does not exist in inventory"}, status=404)


@routes.get('/devices')
async def get_devices(request: web.Request) -> web.Response:
    inventory_list = get_inventory()
    return web.json_response({"devices":inventory_list.get('devices')}, status=200)


@routes.post('/{device}/configuration/interface')
async def post_interface_config(request: web.Request) -> web.Response:
    device = request.match_info.get("device", "")
    response = {}
    try:
        data = await request.json()
        DATA_VALID = True
    except Exception as err:
        data = err
        DATA_VALID = False
    logger.info("JSON payload received: \n{}", data)

    try:
        get_config = request.query['get_config']
    except:
        get_config = False
    logger.info("get config filter {}", get_config)

    try:
        load_config = request.query['load_config']
    except:
        load_config = None
    logger.info("load config filter {}", load_config)

    try:
        commit = request.query['commit']
        load_config=True
    except:
        commit = None


    try:
        rollback_timer = int(request.query['rollback_timer'])
        #rollback_timer = int(rollback_timer_tmp)
    except:
        rollback_timer = None
    logger.info("Rollback timer {}", rollback_timer)

    inv_check, inventory_list = _check_inventory(device)
    if inv_check:
        if DATA_VALID:
            commit_state = False
            try:
                config_dev = ConfigGen(device)
                result_state, config = config_dev.config_render(data)
                logger.info("Config render state {}. Config: \n{}", result_state, config)
                if result_state:
                    if get_config:
                        response['generated_config'] = config
                        
                    if load_config:
                        dev = junos(device, USERNAME, PASSWORD)
                        config_load_state, diff = dev.load_configuration(config)
                        response['changes'] = diff

                        if commit and commit=="confirmed":
                            if rollback_timer and isinstance(rollback_timer, int):
                                commit_state, commit_result = dev.commit_config(confirmed=True, rollback_timer=rollback_timer)
                                response['commit_state'] = commit_state
                                response['commit_result'] = commit_result
                            else:
                                commit_state, commit_result = dev.commit_config(confirmed=True)
                                response['commit_state'] = commit_state
                                response['commit_result'] = commit_result
                        elif commit:
                            commit_state, commit_result = dev.commit_config()
                            response['commit_state'] = commit_state
                            response['commit_result'] = commit_result

                        dev.unlock_config()
                        dev.disconnect()

                    if commit_state:
                        return web.json_response(response, status=200)
                    elif get_config and not load_config and not commit:
                        return web.json_response(response, status=200)
                    else:
                        return web.json_response(response, status=500)
                
                else:
                    return web.json_response({'Error':'Configuration not found'}, status=404)
        
            except Exception as err:
                logger.error("Exception encountered: {}", err)
                return web.json_response({"Error": str(err)}, status=500)
        else:
            return web.json_response({"Error": data}, status=400)
    else:
        return web.json_response({"Error":"Device does not exist in inventory"}, status=404)


@routes.post('/{device}/confirm_commit')
async def post_interface_config(request: web.Request) -> web.Response:
    device = request.match_info.get("device", "")

    inv_check, inventory_list = _check_inventory(device)
    response = {}
    if inv_check:
        try:
            dev = junos(device, USERNAME, PASSWORD)
            dev.lock_config(edit_mode=None)
            commit_state, commit_result = dev.commit_config(comment="Commit confirmed from orch platform")
            response['commit_state'] = commit_state
            response['commit_result'] = commit_result

            dev.unlock_config()
            dev.disconnect()
            return web.json_response(response, status=200)

        except Exception as err:
            return web.json_response({'Error': err}, status=404)
    else:
        return web.json_response({"Error":"Device does not exist in inventory"}, status=404)


@routes.delete('/{device}/interface/{interfaceName}')
async def del_interface_config(request: web.Request) -> web.Response:
    device = request.match_info.get("device", "")
    interfaceName = request.match_info.get("interfaceName").replace("_", "/")


    inv_check, inventory_list = _check_inventory(device)
    response = {}


    try:
        get_config = request.query['get_config']
    except:
        get_config = False
    logger.info("get config filter {}", get_config)

    try:
        commit = request.query['commit']
    except:
        commit = None


    try:
        rollback_timer = int(request.query['rollback_timer'])
        #rollback_timer = int(rollback_timer_tmp)
    except:
        rollback_timer = None
    logger.info("Rollback timer {}", rollback_timer)

    config = f"delete interfaces {interfaceName}"
    if inv_check:
        try:
            if get_config:
                response['generated_config'] = config

            dev = junos(device, USERNAME, PASSWORD)
            config_load_state, diff = dev.load_configuration(config, data_format='set')
            response['changes'] = diff

            if config_load_state:
                if commit and commit=="confirmed":
                    if rollback_timer and isinstance(rollback_timer, int):
                        commit_state, commit_result = dev.commit_config(confirmed=True, rollback_timer=rollback_timer)
                        response['commit_state'] = commit_state
                        response['commit_result'] = commit_result
                    else:
                        commit_state, commit_result = dev.commit_config(confirmed=True)
                        response['commit_state'] = commit_state
                        response['commit_result'] = commit_result
                elif commit:
                    commit_state, commit_result = dev.commit_config()
                    response['commit_state'] = commit_state
                    response['commit_result'] = commit_result

                dev.unlock_config()
                dev.disconnect()

            return web.json_response(response, status=200)
        
        except Exception as err:
            return web.json_response({"Error": err}, status=500)
    else:
        return web.json_response({"Error":"Device does not exist in inventory"}, status=404)

async def init_app() -> web.Application:
    app = web.Application()
    app.add_routes(routes)
    return app

if __name__ == "__main__":
    application = init_app()
    web.run_app(application, port=8080)

