FROM python:3.7-alpine

COPY . /app
WORKDIR /app

RUN apk add --update --no-cache \
        gcc \
        libressl-dev \
        musl-dev \
        libffi-dev \
        libxml2 \
        libxslt \
        libxml2-dev \
        libxslt-dev \
        make \
        cargo
        
COPY requirements.txt /
RUN pip install -r requirements.txt

CMD ["python3", "/app/rest_api/orch.py"]
